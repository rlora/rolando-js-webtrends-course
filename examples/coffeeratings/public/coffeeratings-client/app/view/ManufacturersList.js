/*
 * Copyright (c) 2013 Jalasoft. Web Trends Course.
 * 1219 Durham
 * Houston. Texas.
 *
 * This application was developed as part of the 2013
 * "CURRENT TRENDS IN WEB APPLICATION DEVELOPMENT" course
 * held at Jalasoft Foundation.
 *
 * This software is the confidential and proprietary information of Jalasoft.
 * ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Jalasoft.
 *
 */

/**
 * @class CoffeeRatings.view.ManufacturersList
 */
Ext.define('CoffeeRatings.view.ManufacturersList', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.manufacturerslist',
    xtype: 'manufacturerslist',
    store: 'EspressoMachineManufacturers',
    columns: [{
        text: 'Name',
        dataIndex: 'name',
        renderer: function(value, metaData, record) {
            return (record.raw.isNew ? '<strong style="color: red">' + value + '</strong>' : value);
        }
    }, {
        text: 'Website',
        dataIndex: 'website',
        flex: 1
    }, {
        text: 'Machine Types',
        dataIndex: 'machine_types'
    },
    {
        text: 'Groups',
        dataIndex: 'groups'
    },
    {
        text: 'Models',
        dataIndex: 'models',
        flex: 1
    }],

    /*
     * Initialize component
     */
    initComponent: function() {
        var me = this;
        me.callParent();
    }
});
