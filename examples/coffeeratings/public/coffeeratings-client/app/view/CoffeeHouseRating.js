/*
 * Copyright (c) 2013 Jalasoft. Web Trends Course.
 * 1219 Durham
 * Houston. Texas.
 *
 * This application was developed as part of the 2013
 * "CURRENT TRENDS IN WEB APPLICATION DEVELOPMENT" course
 * held at Jalasoft Foundation.
 *
 * This software is the confidential and proprietary information of Jalasoft.
 * ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Jalasoft.
 *
 */

/**
 * @class CoffeeRatings.view.CoffeeHouseRating
 */
Ext.define('CoffeeRatings.view.CoffeeHouseRating', {
    extend: 'Ext.chart.Chart',
    alias: 'widget.coffeehouserating',
    xtype: 'coffeehouserating',
    insetPadding: 20,
    legend: {
        position: 'bottom'
    },
    store: {
        fields: ['name', 'rating', 'average'],
        data: [{
            name: 'Aroma',
            rating: 9,
            average: 7
        }, {
            name: 'Body',
            rating: 7.5,
            average: 7
        }, {
            name: 'Brightness',
            rating: 8.5,
            average: 7
        }, {
            name: 'Crema',
            rating: 9,
            average: 7
        }, {
            name: 'Flavor',
            rating: 8.5,
            average: 7
        }]
    },
    axes: [{
        type: 'Radial',
        position: 'radial',
        fields: ['rating'],
        minimum: 0,
        maximum: 10,
        majorTickSteps: 1
    }],

    series: [{
        showInLegend: true,
        showMarkers: true,
        type: 'radar',
        xField: 'name',
        yField: 'rating',
        style: {
            opacity: 0.4
        },
        tips: {
            trackMouse: true,
            width: 100,
            height: 28,
            renderer: function(storeItem, item) {
                this.setTitle(storeItem.get('name') + ': ' + storeItem.get('rating'));
            }
        }
    }, {
        showInLegend: true,
        showMarkers: true,
        type: 'radar',
        xField: 'name',
        yField: 'average',
        style: {
            opacity: 0.4
        },
        tips: {
            trackMouse: true,
            width: 100,
            height: 28,
            renderer: function(storeItem, item) {
                this.setTitle(storeItem.get('name') + ': ' + storeItem.get('average'));
            }
        }
    }],

    /*
     * Initialize component
     */
    initComponent: function() {
        var me = this;
        me.callParent();
    }
});
