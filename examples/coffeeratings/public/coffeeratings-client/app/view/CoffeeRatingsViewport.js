/*
 * Copyright (c) 2013 Jalasoft. Web Trends Course.
 * 1219 Durham
 * Houston. Texas.
 *
 * This application was developed as part of the 2013
 * "CURRENT TRENDS IN WEB APPLICATION DEVELOPMENT" course
 * held at Jalasoft Foundation.
 *
 * This software is the confidential and proprietary information of Jalasoft.
 * ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Jalasoft.
 *
 */

/**
 * @class CoffeeRatings.view.CoffeeRatingsViewport
 */
Ext.define('CoffeeRatings.view.CoffeeRatingsViewport', {
    extend: 'Ext.container.Viewport',
    xtype: 'coffeeratingsviewport',
    layout: {
        type: 'hbox',
        pack: 'start',
        align: 'stretch'
    },
    requires: [
        'CoffeeRatings.view.ManufacturersList',
        'CoffeeRatings.view.CoffeeHouseRating'
    ],
    items: [{
        flex: 1,
        xtype: 'container',
        border: false
    }, {
        width: 1024,
        border: false,
        xtype: 'container',
        layout: {
            type: 'hbox',
            pack: 'start',
            align: 'stretch'
        },
        items: [{
            flex: 1,
            xtype: 'manufacturerslist',
            title: 'Manufacturers List'
        }, {
            flex: 1,
            layout: {
                type: 'vbox',
                pack: 'start',
                align: 'stretch'
            },
            items: [{
                height: 250,
                border: false,
                type: 'container',
                layout: {
                    type: 'hbox',
                    pack: 'start',
                    align: 'stretch'
                },
                items: [{
                    flex: 1,
                    border: false,
                    title: 'Coffee Rating',
                    layout: 'fit',
                    items: {
                        xtype: 'coffeehouserating'
                    }
                }, {
                    flex: 1,
                    border: false,
                    title: 'Coffee House Raiting',
                    layout: 'fit',
                    items: {
                        xtype: 'coffeehouserating'
                    }
                }]
            }, {
                flex: 1,
                border: false,
                type: 'container',
                layout: {
                    type: 'hbox',
                    pack: 'start',
                    align: 'stretch'
                },
                items: [{
                    flex: 1,
                    border: false,
                    title: 'Machine'
                }, {
                    flex: 1,
                    border: false,
                    title: 'Beans'
                }, {
                    flex: 1,
                    border: false,
                    title: 'Cups'
                }]
            }, {
                flex: 2,
                border: false,
                type: 'container',
                title: 'Coffee House Info'
            }]
        }]
    }, {
        flex: 1,
        border: false,
        xtype: 'container'
    }],

    /*
     * Initialize component
     */
    initComponent: function() {
        var me = this;
        me.callParent();
    }
});