/*
 * Copyright (c) 2013 Jalasoft. Web Trends Course.
 * 1219 Durham
 * Houston. Texas.
 *
 * This application was developed as part of the 2013
 * "CURRENT TRENDS IN WEB APPLICATION DEVELOPMENT" course
 * held at Jalasoft Foundation.
 *
 * This software is the confidential and proprietary information of Jalasoft.
 * ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Jalasoft.
 *
 */

/**
 * @class CoffeeRatings.controller.EspressoMachineManufacturer
 */
Ext.define('CoffeeRatings.controller.EspressoMachineManufacturer', {
    extend: 'Ext.app.Controller',
    stores: ['CoffeeRatings.store.EspressoMachineManufacturers'],

    refs: [{
        ref: 'list',
        selector: 'manufacturerslist'
    }],

    /*
     * Reference to GAE channel
     */
    _channel: undefined,

    /**
     * Controller's init method
     */
    init: function() {
        this.initializeGAEChannel();
    },

    /**
     * Initializes channel if data attribute is found
     */
    initializeGAEChannel: function() {
        var me = this;
        channelGuid = Ext.query('script[data-channel]');
        if (channelGuid.length === 1) {
            id = channelGuid[0].getAttribute('data-channel');
            me._channel = new goog.appengine.Channel(id);
            socket = me._channel.open();
            socket.onopen = me.channelHandlerOnOpened.bind(me); /* Why we need bind?? */
            socket.onmessage = me.channelHandlerOnMessage.bind(me);
            socket.onerror = me.channelHandlerOnError.bind(me);
            socket.onclose = me.channelHandlerOnClose.bind(me);
        }
    },

    channelHandlerOnOpened: function() {
        /* Empty for now */
    },

    channelHandlerOnMessage: function(msg) {
        var response, me = this;
        if (msg && msg.data) {
            response = Ext.JSON.decode(msg.data);
            me.getList().store.add(response);
        }
    },

    channelHandlerOnError: function() {
        /* Empty for now */
    },

    channelHandlerOnClose: function() {
        /* Empty for now */
    }

});