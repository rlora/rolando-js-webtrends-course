/*
 * Copyright (c) 2013 Jalasoft. Web Trends Course.
 * 1219 Durham
 * Houston. Texas.
 *
 * This application was developed as part of the 2013
 * "CURRENT TRENDS IN WEB APPLICATION DEVELOPMENT" course
 * held at Jalasoft Foundation.
 *
 * This software is the confidential and proprietary information of Jalasoft.
 * ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Jalasoft.
 *
 */

/**
 * @class CoffeeRatings.store.EspressoMachineManufacturers
 */
Ext.define('CoffeeRatings.store.EspressoMachineManufacturers', {
    extend: 'Ext.data.Store',
    requires: 'CoffeeRatings.model.EspressoMachineManufacturer',
    model: 'CoffeeRatings.model.EspressoMachineManufacturer',
    sorters: ['name'],
    autoLoad: true
});