/*
 * Copyright (c) 2013 Jalasoft. Web Trends Course.
 * 1219 Durham
 * Houston. Texas.
 *
 * This application was developed as part of the 2013
 * "CURRENT TRENDS IN WEB APPLICATION DEVELOPMENT" course
 * held at Jalasoft Foundation.
 *
 * This software is the confidential and proprietary information of Jalasoft.
 * ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Jalasoft.
 *
 */

/**
 * @class CoffeeRatings.model.EspressoMachineManufacturer
 */
Ext.define('CoffeeRatings.model.EspressoMachineManufacturer', {
    extend: 'Ext.data.Model',
    fields: ['id', 'name', 'website', 'machine_types', 'models', 'groups'],
    proxy: {
        type: 'rest',
        url: '/espresso_machine_manufacturers',
        format: 'json'
    }
});