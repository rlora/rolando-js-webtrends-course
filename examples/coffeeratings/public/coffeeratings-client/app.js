/*
 * Copyright (c) 2013 Jalasoft. Web Trends Course.
 * 1219 Durham
 * Houston. Texas.
 *
 * This application was developed as part of the 2013
 * "CURRENT TRENDS IN WEB APPLICATION DEVELOPMENT" course
 * held at Jalasoft Foundation.
 *
 * This software is the confidential and proprietary information of Jalasoft.
 * ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Jalasoft.
 *
 */

/**
 * Coffee Ratings App
 */
Ext.application({
    name: 'CoffeeRatings',
    models: ['EspressoMachineManufacturer'],    
    stores: ['EspressoMachineManufacturers'],
    controllers: ['EspressoMachineManufacturer'],
    views: ['CoffeeRatingsViewport'],

    launch: function() {
        Ext.create('CoffeeRatings.view.CoffeeRatingsViewport', {});
    }
});