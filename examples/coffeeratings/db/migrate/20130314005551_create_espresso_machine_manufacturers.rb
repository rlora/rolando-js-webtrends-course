class CreateEspressoMachineManufacturers < ActiveRecord::Migration
  def change
    create_table :espresso_machine_manufacturers do |t|
      t.string :name
      t.string :website
      t.text :machine_types
      t.string :groups
      t.text :models

      t.timestamps
    end
  end
end
