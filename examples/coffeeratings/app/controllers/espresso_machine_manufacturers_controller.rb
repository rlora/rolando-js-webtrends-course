class EspressoMachineManufacturersController < ApplicationController
  # GET /espresso_machine_manufacturers
  # GET /espresso_machine_manufacturers.json
  def index
    @espresso_machine_manufacturers = EspressoMachineManufacturer.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render :json => @espresso_machine_manufacturers }
    end
  end

  # GET /espresso_machine_manufacturers/1
  # GET /espresso_machine_manufacturers/1.json
  def show
    @espresso_machine_manufacturer = EspressoMachineManufacturer.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render :json => @espresso_machine_manufacturer }
    end
  end

  # GET /espresso_machine_manufacturers/new
  # GET /espresso_machine_manufacturers/new.json
  def new
    @espresso_machine_manufacturer = EspressoMachineManufacturer.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render :json => @espresso_machine_manufacturer }
    end
  end

  # GET /espresso_machine_manufacturers/1/edit
  def edit
    @espresso_machine_manufacturer = EspressoMachineManufacturer.find(params[:id])
  end

  # POST /espresso_machine_manufacturers
  # POST /espresso_machine_manufacturers.json
  def create
    @espresso_machine_manufacturer = EspressoMachineManufacturer.new(params[:espresso_machine_manufacturer])

    respond_to do |format|
      if @espresso_machine_manufacturer.save
        format.html { redirect_to @espresso_machine_manufacturer, :notice => 'Espresso machine manufacturer was successfully created.' }
        format.json { render :json => @espresso_machine_manufacturer, :status => :created, :location => @espresso_machine_manufacturer }
      else
        format.html { render :action => "new" }
        format.json { render :json => @espresso_machine_manufacturer.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /espresso_machine_manufacturers/1
  # PUT /espresso_machine_manufacturers/1.json
  def update
    @espresso_machine_manufacturer = EspressoMachineManufacturer.find(params[:id])

    respond_to do |format|
      if @espresso_machine_manufacturer.update_attributes(params[:espresso_machine_manufacturer])
        format.html { redirect_to @espresso_machine_manufacturer, :notice => 'Espresso machine manufacturer was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render :action => "edit" }
        format.json { render :json => @espresso_machine_manufacturer.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /espresso_machine_manufacturers/1
  # DELETE /espresso_machine_manufacturers/1.json
  def destroy
    @espresso_machine_manufacturer = EspressoMachineManufacturer.find(params[:id])
    @espresso_machine_manufacturer.destroy

    respond_to do |format|
      format.html { redirect_to espresso_machine_manufacturers_url }
      format.json { head :no_content }
    end
  end
end
