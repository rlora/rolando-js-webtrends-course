class EspressoMachineManufacturer < ActiveRecord::Base
  attr_accessible :groups, :machine_types, :models, :name, :website
end
