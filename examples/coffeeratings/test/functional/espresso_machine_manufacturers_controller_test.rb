require 'test_helper'

class EspressoMachineManufacturersControllerTest < ActionController::TestCase
  setup do
    @espresso_machine_manufacturer = espresso_machine_manufacturers(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:espresso_machine_manufacturers)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create espresso_machine_manufacturer" do
    assert_difference('EspressoMachineManufacturer.count') do
      post :create, :espresso_machine_manufacturer => { :groups => @espresso_machine_manufacturer.groups, :machine_types => @espresso_machine_manufacturer.machine_types, :models => @espresso_machine_manufacturer.models, :name => @espresso_machine_manufacturer.name, :website => @espresso_machine_manufacturer.website }
    end

    assert_redirected_to espresso_machine_manufacturer_path(assigns(:espresso_machine_manufacturer))
  end

  test "should show espresso_machine_manufacturer" do
    get :show, :id => @espresso_machine_manufacturer
    assert_response :success
  end

  test "should get edit" do
    get :edit, :id => @espresso_machine_manufacturer
    assert_response :success
  end

  test "should update espresso_machine_manufacturer" do
    put :update, :id => @espresso_machine_manufacturer, :espresso_machine_manufacturer => { :groups => @espresso_machine_manufacturer.groups, :machine_types => @espresso_machine_manufacturer.machine_types, :models => @espresso_machine_manufacturer.models, :name => @espresso_machine_manufacturer.name, :website => @espresso_machine_manufacturer.website }
    assert_redirected_to espresso_machine_manufacturer_path(assigns(:espresso_machine_manufacturer))
  end

  test "should destroy espresso_machine_manufacturer" do
    assert_difference('EspressoMachineManufacturer.count', -1) do
      delete :destroy, :id => @espresso_machine_manufacturer
    end

    assert_redirected_to espresso_machine_manufacturers_path
  end
end
