# coding=utf-8
import os
import webapp2
import json
import random
import hashlib
from google.appengine.ext import db
from google.appengine.api import mail
from google.appengine.api import channel
import jinja2
import json

jinja_environment = jinja2.Environment(
  loader = jinja2.FileSystemLoader(os.path.join(os.path.dirname(__file__), 'templates')))

class Customer(db.Model):
  email = db.EmailProperty()
  invited = db.BooleanProperty(default=False)
  created = db.DateTimeProperty(auto_now_add=True)

class CustomersHandler(webapp2.RequestHandler):
    
  def notify(self, email):
    message = mail.EmailMessage()
    message.sender = ''
    message.to = email
    message.subject = ''
    message.body = ''
    message.send()
  
  def get(self):
    response = {"success": True}
    email = db.Email(self.request.get('email'))
    customer = Customer(email=email.lower().strip())
    customer.put()
    self.response.write(json.dumps(response))

class CoffeeRatingsClientHandler(webapp2.RequestHandler):
  def get(self):
    try:
      private_token = hashlib.md5(str(random.random())).hexdigest()
      token = channel.create_channel(private_token)
      template = jinja_environment.get_template('coffeeratings-client.html')
      self.response.out.write(template.render({'token': token, 'private_token': private_token}))
    except:
      template = jinja_environment.get_template('404.html')
      self.response.write(template.render({}))
      self.response.set_status(404)

  def put(self):
    try:
      private_token = self.request.get('private_token')
      name = self.request.get('name')
      response = {
        'success': True,
        'name': name,
        'isNew': True
      }
      channel.send_message(private_token, json.dumps(response))
      self.response.write(json.dumps(response))
    except:
      self.response.write(json.dumps({'success': False}))
