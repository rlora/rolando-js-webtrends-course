import logging
import os
import webapp2
import jinja2

jinja_environment = jinja2.Environment(
  loader = jinja2.FileSystemLoader(os.path.join(os.path.dirname(__file__), 'templates')))

def handle_500(request, response, exception):
  template = jinja_environment.get_template('500.html')
  response.write(template.render({}))
  response.set_status(500)

class HomeHandler(webapp2.RequestHandler):
  def get(self):
    template = jinja_environment.get_template('index.html')
    self.response.write(template.render({}))

class ViewHandler(webapp2.RequestHandler):
  def get(self, view):    
    try:
      extension = '' if self.request.path.endswith('json') else '.html'
      template = jinja_environment.get_template(self.request.path.replace('/', '') + extension)
      self.response.out.write(template.render({}))
    except:
      template = jinja_environment.get_template('404.html')
      self.response.write(template.render({}))
      self.response.set_status(404)

app = webapp2.WSGIApplication()
app.router.add((r'/', HomeHandler))
app.router.add((r'/resources/customers', 'resources.CustomersHandler'))
app.router.add((r'/coffeeratings-client', 'resources.CoffeeRatingsClientHandler'))
app.router.add((r'/([^/]+)', ViewHandler))